#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"

int main(){
    int c;
    struct chaine ch;
    init_chaine(&ch);
    c=getchar();
    while(!isspace(c)){
        ajout_chaine(&ch,c);
        c=getchar();
    }
    affichage_chaine(&ch);
    clear_chaine(&ch);
    return 0;
}    
