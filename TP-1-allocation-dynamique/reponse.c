// Programme
/*
#include <stdio.h>
#include <ctype.h>
int main (){
    int c;
    c = getchar ();
    while (!isspace (c)){
        putchar (c);
        c = getchar ();
    }
    putchar ('\n');
    return 0;
}
*/

//Q1)
/*
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main (){
    int c,i,j;
    char *s;
    i=0;
    s=(char *)malloc(64*sizeof(char));
    c = getchar ();
    while ((!isspace (c)) && (i<64)){
        s[i]=c;
        c = getchar ();
        i=i+1;
    }
    for (j=0;j<i;j++){
        putchar(s[j]);
    }
    putchar('\n');
    free(s);
    return 0;
}
*/
//Aucune fuite de mémoire d'après Valgrind

//Q2)
/*
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

int main (){
    int c,i,j,n;
    char *s;
    n=0;
    i=0;
    s=(char *)malloc(4*sizeof(char));
    c = getchar ();
    while (!isspace (c)){
        s[i]=c;
        i=i+1;
        c = getchar ();
        if ((i>4+(8*n)) && (!isspace(c))){
            n=n+1;
            s=(char *) realloc(s,(4+8*n)*sizeof(char));
        }
    }
    for (j=0;j<i;j++){
        putchar(s[j]);
    }
    putchar('\n');
    free(s);
    return 0;
}
*/
//Q3)

//Voir fichiers main.c, chaine.c, chaine.h



