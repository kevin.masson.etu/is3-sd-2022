#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include "chaine.h"


/* Constructeur */

void init_chaine(struct chaine *ch){
    (*ch).s=malloc(4*sizeof(char)); //ou ch->s=...
    (*ch).size=0;
}

void ajout_chaine(struct chaine *ch, int c){
    (*ch).s[(*ch).size]=c;
    (*ch).size=(*ch).size+1;
    c = getchar ();
    if (((*ch).size>(*ch).size_alloue) && (!isspace(c))){
        (*ch).size_alloue=(*ch).size_alloue+8;
        (*ch).s=realloc((*ch).s,8*sizeof(char));
    }
}

void affichage_chaine(struct chaine *ch){
    int i=0;
    for (i=0;i<(*ch).size;i++){
        putchar((*ch).s[i]);
    }
    putchar('\n');
}


/* Destructeur */

void clear_chaine(struct chaine *ch){
    free((*ch).s);
}
