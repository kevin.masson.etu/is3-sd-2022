/* Implantation du type chaine */

struct chaine{
    char *s;
    int size;
    int size_alloue;
};

/* Prototypes des fonctions exportées */

extern void init_chaine(struct chaine *ch);

extern void ajout_chaine(struct chaine *ch, int c);

extern void affichage_chaine(struct chaine *ch);

extern void clear_chaine(struct chaine *ch);
