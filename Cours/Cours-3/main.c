// main.c

#include "liste.h"

int main ()
  {
    struct liste L0;  // dans la pile !

    init_liste (&L0);
    ajout_en_tete (&L0, 23);
    ajout_en_tete (&L0, 14);
    ajout_en_tete (&L0, 17);
    impression (L0);
    clear_liste (&L0);
    return 0;
  }

