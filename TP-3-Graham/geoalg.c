/*
 * Interface Python - C du TP.
 * Ce fichier ne concerne que la dernière section du TP.
 * Il ne comporte aucun travail à faire.
 */

#include <Python.h>
#include <structmember.h>
#include "Graham.h"

/*
 * Un struct Graham est un objet Python qui encapsule 
 * un tableau redimensionnable de points
 */

struct Graham
{
  PyObject_HEAD 
  int alloc;
  int size;
  struct point *T;
};

/*
 * Le destructeur + free. En C++ ce serait delete
 */

static void
Graham_dealloc (struct Graham *self)
{
  printf ("Appel au destructeur de Graham\n");
  free (self->T);
  Py_TYPE (self)->tp_free ((PyObject *) self);
}

/*
 * En C++, new = malloc + constructeur
 * En Python, le constructeur sera appelé séparément donc new = malloc
 */

static PyObject *
Graham_new (PyTypeObject * type, PyObject * args, PyObject * kwds)
{
  struct Graham *self;

  printf ("Appel à new Graham\n");
  self = (struct Graham *) type->tp_alloc (type, 0);
  return (PyObject *) self;
}

/*
 * Le constructeur
 */

static int
Graham_init (struct Graham *self, PyObject * args, PyObject * kwds)
{
  printf ("Appel au constructeur par défaut de Graham\n");
  self->alloc = 0;
  self->size = 0;
  self->T = (struct point *) 0;
  return 0;
}

/*
 * Remplit self avec des points aléatoires
 */

static PyObject *
points_aleatoires (struct Graham *self, PyObject * args, PyObject * kw)
{
  static char *kwlist[] = { "n", "seed", NULL };
  int n = -1;
  int seed = -1;

  if (!PyArg_ParseTupleAndKeywords (args, kw, "i|i", kwlist, &n, &seed))
    return NULL;

  if (n < 3 || n > 26)
    {
      PyErr_SetString (PyExc_RuntimeError,
                       "le premier argument doit vérifier : 3 <= n <= 26");
      return NULL;
    }

  if (n > self->alloc)
    {
      self->T = (struct point *) realloc (self->T, n * sizeof (struct point));
      self->alloc = n;
    }

  if (seed != -1)
    {
      printf
        ("initialisation du générateur pseudo-aléatoire à : %d\n", seed);
      srand48 (seed);
    }

  init_point (&self->T[0], 0, 0, 'A');
  for (self->size = 1; self->size < n; self->size++)
    {
      double x, y;
      x = drand48 ();
      y = drand48 ();
      init_point (&self->T[self->size], x, y, 'A' + self->size);
    }

  if (!trie_points (self->size - 1, self->T + 1))
    {
      self->size = 0;
      PyErr_SetString (PyExc_RuntimeError,
                       "alignement de points non traité");
      return NULL;
    }

  Py_RETURN_NONE;
}

/*
 * Retourne la liste des points contenus dans self
 */

static PyObject *
get_points (struct Graham *self)
{
  PyObject *L;

  L = PyList_New (self->size);
  Py_INCREF (L);
  for (int i = 0; i < self->size; i++)
    {
      PyObject *M, *x, *y, *z;
      char buffer[2];

      x = PyFloat_FromDouble (self->T[i].x);
      Py_INCREF (x);
      y = PyFloat_FromDouble (self->T[i].y);
      Py_INCREF (y);
      buffer[0] = self->T[i].ident;
      buffer[1] = '\0';
      z = PyUnicode_FromString (buffer);
      Py_INCREF (z);
      M = PyList_New (3);
      Py_INCREF (M);
      PyList_SetItem (M, 0, x);
      PyList_SetItem (M, 1, y);
      PyList_SetItem (M, 2, z);
      PyList_SetItem (L, i, M);
    }

  return L;
}

/*
 * Retourne l'enveloppe convexe des points contenus dans self
 */

static PyObject *
get_enveloppe (struct Graham *self)
{
  struct point *envlp;
  int nb_envlp;

  if (self->size < 3)
    {
      PyErr_SetString (PyExc_RuntimeError,
                       "trop peu de points sont définis pour calculer l'enveloppe");
      return NULL;
    }

  envlp = (struct point *) malloc (self->size * sizeof (struct point));

  Graham (&nb_envlp, envlp, self->size, self->T);

  PyObject *R;

  R = PyList_New (0);
  Py_INCREF (R);
  for (int i = 0; i < nb_envlp; i++)
    {
      PyObject *M, *x, *y;

      x = PyFloat_FromDouble (envlp[i].x);
      Py_INCREF (x);
      y = PyFloat_FromDouble (envlp[i].y);
      Py_INCREF (y);
      M = PyList_New (2);
      Py_INCREF (M);
      PyList_SetItem (M, 0, x);
      PyList_SetItem (M, 1, y);
      PyList_Append (R, M);
    }

  free (envlp);

  return R;
}

/*
 * Le tableau des méthodes
 */

static PyMethodDef Graham_methods[] = {
  {"points_aleatoires",
   (PyCFunction) points_aleatoires,
   METH_VARARGS | METH_KEYWORDS, "points_aleatoires"},
  {"get_points",
   (PyCFunction) get_points,
   METH_NOARGS, "get_points"},
  {"get_enveloppe",
   (PyCFunction) get_enveloppe,
   METH_NOARGS, "get_enveloppe"},
  {NULL}                        /* Sentinel */
};

/*
 * Le type Graham
 */

static PyTypeObject geoalg_GrahamType = {
  PyVarObject_HEAD_INIT (NULL, 0) "geoalg.Graham",      /* tp_name */
  sizeof (struct Graham),       /* tp_basicsize */
  0,                            /* tp_itemsize */
  (destructor) Graham_dealloc,  /* tp_dealloc */
  0,                            /* tp_print */
  0,                            /* tp_getattr */
  0,                            /* tp_setattr */
  0,                            /* tp_reserved */
  0,                            /* tp_repr */
  0,                            /* tp_as_number */
  0,                            /* tp_as_sequence */
  0,                            /* tp_as_mapping */
  0,                            /* tp_hash  */
  0,                            /* tp_call */
  0,                            /* tp_str */
  0,                            /* tp_getattro */
  0,                            /* tp_setattro */
  0,                            /* tp_as_buffer */
  Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,     /* tp_flags */
  "Graham objet",               /* tp_doc */
  0,                            /* tp_traverse */
  0,                            /* tp_clear */
  0,                            /* tp_richcompare */
  0,                            /* tp_weaklistoffset */
  0,                            /* tp_iter */
  0,                            /* tp_iternext */
  Graham_methods,               /* tp_methods */
  0,                            /* tp_members */
  0,                            /* tp_getset */
  0,                            /* tp_base */
  0,                            /* tp_dict */
  0,                            /* tp_descr_get */
  0,                            /* tp_descr_set */
  0,                            /* tp_dictoffset */
  (initproc) Graham_init,       /* tp_init */
  0,                            /* tp_alloc */
  Graham_new,                   /* tp_new */
};

/*
 * Le module geoalg auquel le type Graham appartient
 */

static PyModuleDef geoalgmodule = {
  PyModuleDef_HEAD_INIT,
  "geoalg",
  "Définit le type Graham",
  -1,
  NULL, NULL, NULL, NULL, NULL
};

/*
 * Fonction qui initialise le module
 */

PyMODINIT_FUNC
PyInit_geoalg (void)
{
  PyObject *m;

  if (PyType_Ready (&geoalg_GrahamType) < 0)
    return NULL;

  m = PyModule_Create (&geoalgmodule);
  if (m == NULL)
    return NULL;
/*
 * Pas très propre parce que geoalg_GrahamType est une structure
 * qui appartient à la zone des variables globales initialisées (data)
 * et pas au tas : l'incrémentation du compteur de référence a 
 * pour effet d'éviter un appel fautif au destructeur
 */
  Py_INCREF (&geoalg_GrahamType);
  PyModule_AddObject (m, "Graham", (PyObject *) & geoalg_GrahamType);
  return m;
}
